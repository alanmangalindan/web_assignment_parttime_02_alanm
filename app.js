var express = require('express');

var fs = require('fs');

var formidable = require("formidable");

var app = express();

app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

var session = require('express-session');
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: "CS719"
}));


function ensureArray(value) {
    if (value == undefined) return [];
    else return Array.isArray(value) ? value : [value];
}

app.get('/', function (req, res) {

    var allEmployees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));

    if (req.query.sort == "ascending") {

        allEmployees.sort(function (a, b) { return a.lname > b.lname }); // sort ascending

    } else if (req.query.sort == "descending") {

        allEmployees.sort(function (a, b) { return a.lname < b.lname }); // sort descending

    }

    var data = {
        pageTitle: "Staff Directory",
        employees: allEmployees
    }

    console.log(allEmployees);

    res.render('staffDirectory', data);

});

app.get('/remove', function (req, res) {

    var allEmployees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));

    if (req.query.toRemove) {

        var employeesToRemove = ensureArray(req.query.toRemove);

        for (var i = 0; i < employeesToRemove.length; i++) {

            for (var j = 0; j < allEmployees.length; j++) {
                if (employeesToRemove[i] == allEmployees[j].id) {
                    allEmployees.splice(j, 1);
                    break;
                }
            }
        }
    }

    fs.writeFileSync(__dirname + "/employees.json", JSON.stringify(allEmployees));

    res.redirect("/");

});

app.get('/add', function (req, res) {

    var partialEmployee = req.session["partialEmployee"];

    // replace spaces with '&nbsp;' so handlebars will not trim it when it encounters a space
    if (partialEmployee) {
        for (var property in partialEmployee) {
            console.log(partialEmployee[property]);
            var string = partialEmployee[property];
            string = string.toString();

            string = string.replace(/\s/g, "&nbsp;");

            partialEmployee[property] = string;
        }
    }

    var data = partialEmployee;

    res.render('addStaff', data);

});

app.post('/add', function (req, res) {

    var allEmployees = JSON.parse(fs.readFileSync(__dirname + "/employees.json"));

    var empIds = [];

    for (var i = 0; i < allEmployees.length; i++) {
        empIds.push(allEmployees[i].id);
    }
    empIds.sort(function (a, b) { return a - b });

    console.log(empIds);

    // ensure there is no duplication of employee ID
    var newEmpId = 0;

    for (var j = 0; j < empIds.length; j++) {
        if (newEmpId != empIds[j]) {
            break;
        } else {
            newEmpId++;
        }
    }

    var newEmployee = {
        id: newEmpId,
        lname: req.body.lname,
        fname: req.body.fname,
        email: req.body.email,
        ph: req.body.ph,
        address: req.body.address,
        title: req.body.title
    }

    var completeDetails = true;

    for (var property in newEmployee) {
        if (newEmployee[property] == "") {
            completeDetails = false;
            break;
        }
    }

    if (completeDetails) {

        console.log(newEmployee);

        allEmployees.push(newEmployee);

        fs.writeFileSync(__dirname + "/employees.json", JSON.stringify(allEmployees));

        if (req.session["partialEmployee"]) {
            delete req.session["partialEmployee"];
        }

        res.redirect("/");

    } else {

        req.session["partialEmployee"] = newEmployee;
        console.log(req.session);
        res.redirect("/add");
    }

});

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});